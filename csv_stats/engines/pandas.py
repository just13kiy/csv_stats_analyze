import typing as t
from pathlib import Path

import numpy as np
import pandas as pd

from .base import BaseEngine


class PandasEngine(BaseEngine):
    """
    This engine reads whole .csv file at once, so this is good for file within 1-10 million rows
    """
    dtypes = {
        'Row ID': np.int32,
        'Sales': np.float32,
        'Quantity': np.float32,
        'Discount': np.float32,
        'Profit': np.float32,
    }

    def __init__(self):
        pd.set_option('display.precision', 2)
        self._df = pd.DataFrame()

    def load_csv(self, path: str):
        self._df = pd.read_csv(
            path,
            decimal=',',
            sep=';',
            index_col=0,
            dtype=self.dtypes,
            parse_dates=['Order Date', 'Ship Date'],
        )

    def to_csv(self, data: t.Dict[str, t.Any], output_file: t.Union[str, Path]):
        output_df = pd.DataFrame.from_dict(data, orient='index')
        output_df.to_csv(output_file)

    def best_product_by_attr(self, attr_name: str) -> t.Tuple[str, t.Union[int, float]]:
        group_by_product = self._df.groupby('Product ID')[attr_name].sum()
        best_value = group_by_product.max()
        best_id = group_by_product.idxmax()
        return best_id, best_value

    def worst_product_by_attr(self, attr_name: str) -> t.Tuple[str, t.Union[int, float]]:
        group_by_product = self._df.groupby('Product ID')[attr_name].sum()
        worst_value = group_by_product.min()
        worst_id = group_by_product.idxmin()
        return worst_id, worst_value

    def _compute_arrival_time(self) -> pd.DataFrame:
        """
        Compute arrival time and return new DataFrame in case to not affect original DataFrane.
        """
        new_df = self._df.copy()
        new_df['Arrival'] = new_df.apply(lambda row: row['Ship Date'] - row['Order Date'], axis=1)
        return new_df

    def mean_arrival_time(self) -> t.Dict[str, np.timedelta64]:
        df = self._compute_arrival_time()
        group_by_product = df.groupby('Product ID')['Arrival']
        mean = group_by_product.apply(lambda x: np.mean(x))
        return dict(mean.T)

    def std_deviation_arrival_time(self, ) -> t.Dict[str, np.timedelta64]:
        df = self._compute_arrival_time()
        group_by_product = df.groupby('Product ID')['Arrival']
        std = group_by_product.apply(lambda x: np.std(x))
        return dict(std.T)

    def sales_stats(self) -> t.Dict[str, float]:
        group_by_product = self._df.groupby('Product ID')[['Sales', 'Quantity', 'Profit']].sum()
        return dict(group_by_product.T)

    def total_profit(self) -> float:
        profit = self._df['Profit'].sum()
        return profit
