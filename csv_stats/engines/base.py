import typing as t


class BaseEngine:
    """
    Abstract engine class, that describe interface for solving the task's problem.
    """
    def load_csv(self, path: str):
        """
        Should be used to load all data from the file to engine.
        Implementation depends on how particular engine will store this data.
        """
        raise NotImplementedError

    def to_csv(self, data: t.Any, output_file: str):
        """
        Simply dumps given data to the output file.
        If filename is not given, should use 'output_stats.csv' name.
        If file with given filename exists, should rewrite it.
        """
        raise NotImplementedError

    def best_product_by_attr(self, attr_name: str) -> t.Tuple[str, t.Union[int, float]]:
        """
        Takes attribute name and returns the `Product ID` with max value of `attr_name` and the value itself.
        """
        raise NotImplementedError

    def worst_product_by_attr(self, attr_name: str) -> t.Tuple[str, t.Union[int, float]]:
        """
        Takes attribute name and returns the `Product ID` with min value of `attr_name` and the value itself.
        """
        raise NotImplementedError

    def mean_arrival_time(self) -> t.Dict[str, float]:
        """
        Calculate all arrival times and return mean for all products
        """
        raise NotImplementedError

    def std_deviation_arrival_time(self) -> t.Dict[str, float]:
        """
        Calculate all arrival times and return standard deviation for all products
        """
        raise NotImplementedError

    def sales_stats(self) -> t.Dict[str, float]:
        """
        Return `Sales`, `Quantity` and `Profit` stats for every product
        """
        raise NotImplementedError

    def total_profit(self) -> float:
        """
        Sums all profit from all orders
        """
        raise NotImplementedError
