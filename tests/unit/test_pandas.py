import pytest
from pandas import Timedelta

from csv_stats import PandasEngine


@pytest.fixture
def engine():
    return PandasEngine()


@pytest.fixture
def loaded_engine(data_dir):
    engine = PandasEngine()
    engine.load_csv(data_dir / 'data_002.csv')
    return engine


class TestPandasEngine:
    def test_init(self):
        solver = PandasEngine()
        assert solver._df.empty

    def test_load_csv(self, data_dir, engine):
        engine.load_csv(data_dir / 'data_001.csv')
        assert len(engine._df) == 3
        assert engine._df['Order ID'].last_valid_index() == 3

    def test_to_csv(self, tmpdir, engine):
        test_file = tmpdir.join('test.csv')
        data = {
            'ping': ['pong'],
        }
        engine.to_csv(data=data, output_file=test_file)
        assert test_file.read() == ',0\nping,pong\n'

    @pytest.mark.parametrize('attr_name, expected', [
        ('Profit', (2, 7.0)),
        ('Sales', (2, 4.0)),
        ('Quantity', (2, 4.0)),
    ])
    def test_best_product_by_attr(self, data_dir, loaded_engine, attr_name, expected):
        result = loaded_engine.best_product_by_attr(attr_name)
        assert expected == result

    @pytest.mark.parametrize('attr_name, expected', [
        ('Profit', (1, 4.0)),
        ('Sales', (1, 3)),
        ('Quantity', (1, 3.0)),
    ])
    def test_worst_product_by_attr(self, data_dir, loaded_engine, attr_name, expected):
        result = loaded_engine.worst_product_by_attr(attr_name)
        assert expected == result

    def test_mean(self, data_dir, loaded_engine):
        expected = {
            1: Timedelta('3 days 00:00:00'),
            2: Timedelta('4 days 00:00:00')
        }

        result = loaded_engine.mean_arrival_time()
        assert expected == result

    def test_std(self, data_dir, loaded_engine):
        expected = {
            1: Timedelta('0 days 00:00:00'),
            2: Timedelta('0 days 00:00:00')
        }

        result = loaded_engine.std_deviation_arrival_time()
        assert expected == result

    def test_total_profit(self, data_dir, loaded_engine):
        expected = 11.0
        result = loaded_engine.total_profit()
        assert result == expected
