import pathlib

import pytest


@pytest.fixture
def data_dir():
    from . import DATA_DIR
    return pathlib.Path(DATA_DIR)
