import typing as t

import fire

from csv_stats import PandasEngine

if t.TYPE_CHECKING:
    from pathlib import Path


class Solver:
    def __init__(self,
                 input_file: t.Optional[t.Union[str, 'Path']] = 'tests/data/Orders.csv',
                 output_file: t.Optional[t.Union[str, 'Path']] = 'sales_stats.csv',
                 ):
        self._engine = PandasEngine()
        self._engine.load_csv(input_file)
        self.output_file = output_file

    def print_total_profit(self):
        profit = self._engine.total_profit()

        result_msg = f'Total profit from given file: {profit}'
        print(result_msg)

    def print_best(self):
        sales = self._engine.best_product_by_attr('Sales')
        quantity = self._engine.best_product_by_attr('Quantity')
        profit = self._engine.best_product_by_attr('Profit')

        result_msg = f'Best by:\nSales - {sales}\nQuantity - {quantity}\nProfit - {profit}'
        print(result_msg)

    def print_worst(self):
        sales = self._engine.worst_product_by_attr('Sales')
        quantity = self._engine.worst_product_by_attr('Quantity')
        profit = self._engine.worst_product_by_attr('Profit')

        result_msg = f'Worst by:\nSales - {sales}\nQuantity - {quantity}\nProfit - {profit}'
        print(result_msg)

    def print_mean(self):
        mean_arrival = self._engine.mean_arrival_time()

        result_msg = f'Mean time of arriving by each product:\n{mean_arrival}'
        print(result_msg)

    def print_std(self):
        std_arrival = self._engine.std_deviation_arrival_time()

        result_msg = f'Standard deviation of time of arriving by each product:\n{std_arrival}'
        print(result_msg)

    def write_sales_stats_to_file(self):
        stats = self._engine.sales_stats()
        self._engine.to_csv(stats, self.output_file)

        print(f'Sales stats were written to the file: {self.output_file}')

    def calculate_all_stats(self):
        self.print_total_profit()
        self.print_best()
        self.print_worst()
        self.print_mean()
        self.print_std()
        self.write_sales_stats_to_file()


if __name__ == '__main__':
    fire.Fire(Solver)
