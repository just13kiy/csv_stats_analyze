# CSV Stats Analyze (0.2.0)
Package for computing some stats from given .csv file

## Contents
* [Overview](#overview)
* [Installation](#installation)
* [Usage](#usage)

## Overview
Package is consists of two parts: Main package and CLI.
The reason for this decision was the desire to be able to use the instrument from both via CLI and other code.

Main package can compute some statistics from .csv file with certain structure.
In the future, I plan to make it possible to configure computation process and give more 
flexibility to this package.

As for now, there is only one engine to use - Pandas. This is good and fast, but I think that in case of really BIG DATA
(like 1TB or so) one should implement SQL engine to be able to collect stats.

## Installation

### With pip

    pip install git+ssh://git@gitlab.com:just13kiy/csv_stats_analyze.git#egg=csv_stats_analyze
    
### From source

    git clone git@gitlab.com:just13kiy/csv_stats_analyze.git
    poetry install

## Usage
    $ cd csv_stats_analyze\
    # This would show help message
    $ python solver.py
    # This would run all computations that were demand by task
    $ python solver.py calculate_all_stats
    # To specify input file one should use --input_fileflag 
    # To specify output file (only for last task) one should use --output_file flag 